import { RSNetEvent, RSNetworkError, WSCloseCode } from "./types.ts"

const getSubnetIP = (i: number, j: number) => `172.20.${i}.${j}`
const macRegex = /^(?:[0-9A-Z]{2}:){5}[0-9A-Z]{2}$/
const ipMap: Record<string, string> = {}

export class RSNetClient {
	socket: WebSocket
	ip?: string
	mac?: string
	lastKeepalive?: number

	constructor(socket: WebSocket) {
		this.socket = socket
	}

	send(event: RSNetEvent, data: Record<string, unknown>) {
		this.socket.send(JSON.stringify({ event, ...data }))
	}
}

const clients: RSNetClient[] = []

export const registerClient = (socket: WebSocket, mac: string) => {
	const client = new RSNetClient(socket)
	mac = mac.toUpperCase()
	const regexResult = macRegex.exec(mac)
	if (!regexResult) {
		client.socket.close(WSCloseCode.INVALID_DATA, "Invalid MAC provided")
		return undefined
	}
	client.mac = mac
	// check if mac is taken
	if (clients.filter((client) => client.mac == mac).length != 0) {
		// if mac is taken, close conn
		client.socket.close(WSCloseCode.ALREADY_EXISTS, "Client with this MAC already exists")
		return undefined
	}

	console.log(`Client with MAC ${mac} connected.`)

	clients.push(client)
	keepalive(client)
	macToIP(client)
	return client
}

const keepalive = (client: RSNetClient) => {
	if (client.socket.readyState == WebSocket.CLOSED) return
	if (!client.lastKeepalive) client.lastKeepalive = +new Date()
	if (+new Date() - client.lastKeepalive > 31 * 1000) {
		handleClose(client, WSCloseCode.NO_KEEPALIVE, "Did not receive keepalive")
		return
	}
	setTimeout(() => keepalive(client), 30 * 1000)
}

export const handleKeepalive = (client: RSNetClient) => {
	client.lastKeepalive = +new Date()
	client.send(RSNetEvent.KEEPALIVE, { timestamp: client.lastKeepalive })
}

export const handleClose = (client: RSNetClient, reason?: WSCloseCode, reasonDescription?: string) => {
	if (!clients.includes(client)) return
	console.log(`Client with MAC ${client.mac} disconnected.`, reasonDescription ? `(${reasonDescription})` : "")
	client.socket.close(reason ?? 1000, reasonDescription)
	clients.splice(clients.indexOf(client), 1)
}

const macToIP = (client: RSNetClient) => {
	if (!client.mac) return
	// get IP from MAC->IP map
	let ip = ipMap[client.mac]
	if (!ip) {
		// first time we're seeing this MAC, allocate a random IP
		while (ip === undefined) {
			const newIP = getSubnetIP(Math.round(Math.random() * 255), Math.round(Math.random() * 255))
			// if newIP isn't used, assign it
			if (!Object.values(ipMap).includes(newIP)) {
				ipMap[client.mac] = newIP
				ip = newIP
			}
		}
	}
	client.ip = ip
	console.log(`Assigned IP ${ip} to client with MAC ${client.mac}`)
	client.send(RSNetEvent.REGISTER, { ip })
}

export const proxyEvent = (client: RSNetClient, dest: string, type: "ping" | "packet", data: string, port?: number) => {
	const destClient = clients.filter((client) => client.ip == dest)[0]
	if (!destClient) {
		return client.send(RSNetEvent.NETWORK_ERROR, { error: RSNetworkError.DEST_NOT_FOUND })
	}
	destClient.send(type === "ping" ? RSNetEvent.PING : RSNetEvent.PACKET, {
		source: client.ip,
		data,
		port,
	})
}
