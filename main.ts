#!/usr/bin/env -S deno run --allow-net
import { serve } from "https://deno.land/std@0.167.0/http/server.ts"
import { handleClose, handleKeepalive, proxyEvent, registerClient, RSNetClient } from "./protocol.ts"
import { protocolVersion, RSNetEvent, WSCloseCode } from "./types.ts"

const port = 7840

const handler = (request: Request): Response => {
	if (request.url.endsWith("/ws")) {
		const { socket, response } = Deno.upgradeWebSocket(request)
		let client: RSNetClient | undefined

		socket.onopen = () => {
			socket.send(JSON.stringify({ event: RSNetEvent.WELCOME, version: protocolVersion }))
		}

		socket.onmessage = (e) => {
			let data: { event: RSNetEvent; [x: string]: unknown }
			try {
				data = JSON.parse(e.data)
			} catch {
				return socket.close(WSCloseCode.INVALID_DATA, "Invalid data.")
			}

			if (data.event === undefined) return socket.close(WSCloseCode.INVALID_DATA, "Invalid data")

			switch (data.event) {
				case RSNetEvent.REGISTER:
					if (!data.mac) return socket.close(WSCloseCode.INVALID_DATA, "Invalid data")
					client = registerClient(socket, data.mac as string)
					break
				case RSNetEvent.KEEPALIVE:
					if (!client) return socket.close(WSCloseCode.REGISTER_REQUIRED, "Client must register first")
					handleKeepalive(client)
					break
				case RSNetEvent.PING:
					if (!client) return socket.close(WSCloseCode.REGISTER_REQUIRED, "Client must register first")
					proxyEvent(client, data.dest as string, "ping", data.data as string)
					break
				case RSNetEvent.PACKET:
					if (!client) return socket.close(WSCloseCode.REGISTER_REQUIRED, "Client must register first")
					proxyEvent(client, data.dest as string, "packet", data.data as string, data.port as number)
					break
			}
		}

		socket.onerror = (e) => console.warn("WS error:", e)
		socket.onclose = () => client && handleClose(client)

		return response
	}

	return new Response("woof woof rsnet is running", { status: 200 })
}

await serve(handler, { port })
